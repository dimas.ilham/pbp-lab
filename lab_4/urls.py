from django.urls import path
from .views import index, add_note, note_list

# To add path through the url
urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add'),
    path('note-list', note_list, name='list')
]