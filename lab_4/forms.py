from django import forms
from django.forms import ModelForm
from lab_2.models import Note

from django.utils.translation import gettext_lazy as _

# Create your models here.
class NoteForm(ModelForm):
    class Meta :
        model = Note
        fields=['to_person', 'from_person', 'title', 'message']
        labels={
            'to_person':_('To'),
            'from_person':_('From'),
            'title':_('Title'),
            'message':_('Message')
        }