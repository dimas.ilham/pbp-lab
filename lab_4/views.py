from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_2.models import Note
from .forms import NoteForm

# index Function
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

#add_note function
@login_required(login_url="/admin/login/")
def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/lab-4')  # Redirect on finish
        
    else: # if a GET (or any other method) we'll create a blank form
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})

# note-list function
def note_list(request) :
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)