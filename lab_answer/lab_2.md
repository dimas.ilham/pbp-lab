## S

#### Apakah perbedaan antara JSON dan XML?
| JSON                           | XML                            |
| ------------------------------ | ------------------------------ |
| JavaScript Object Notation | Extensible Markup Language |
| berbasis JavaScript | Diturunkan dari SGML (Standard Generalized Markup Language) |
| Salah satu cara untuk merepresentasikan object | Karena merupakan markup language, menggunakan struktur tags untuk mereprensentasikan data item |
| Lebih mudah dibaca dibandingkan XML | Sulit untuk dibaca, atau bahkan diimplementasikan |
| Kurang aman dibandingkan XML | Lebih aman dibandingkan JSON | 

#### Apakah perbedaan antara HTML dan XML?
| HTML                           | XML                            |
| ------------------------------ | ------------------------------ |
| HTML merupakan singkatan dari Hyper Text Markup Language | XML merupakan singkatan dari Extensible Markup Language |
| Statis, karena hanya digunakan untuk menampilkan data | Dinamis, karena digunakan untuk mentransfer data |
| Tags yang digunakan terbatas karena bawaan dari HTML sendiri | Tags yang digunakan dapat diperluas karena tags dapat di-define oleh user |
| Tags tidak case sensitive | Tags case sensitive |