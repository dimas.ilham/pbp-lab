// @dart=2.9
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MaterialApp(
    title: 'Stu-Do-List',
    theme: ThemeData(
      scaffoldBackgroundColor: Colors.blue.shade50,
    ),
    home: MyHomePage(),
  ));
}

class MyHomePage extends StatefulWidget {
  @override
  MyHomePageState createState() {
    return MyHomePageState();
  }
}

// This widget is the home page of your application. It is stateful, meaning
// that it has a State object (defined below) that contains fields that affect
// how it looks.

// This class is the configuration for the state. It holds the values (in this
// case the title) provided by the parent (in this case the App widget) and
// used by the build method of the State. Fields in a Widget subclass are
// always marked "final".

class MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // Return
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Stu-Do-List"),
        backgroundColor: Colors.grey[900],
        foregroundColor: Colors.white,
      ),

      // Sidebar menu
      // https://api.flutter.dev/flutter/material/Drawer-class.html
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.grey[900],
              ),
              child: const Text(
                'Stu-Do-List',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            const ListTile(
              leading: Icon(IconData(58136, fontFamily: 'MaterialIcons')),
              title: Text('Home'),
            ),
            const ListTile(
              leading: Icon(IconData(58950, fontFamily: 'MaterialIcons')),
              title: Text('Agenda'),
            ),
            const ListTile(
              leading: Icon(IconData(58618, fontFamily: 'MaterialIcons')),
              title: Text('Ask a Mentor'),
            ),
            const ListTile(
              leading: Icon(IconData(58710, fontFamily: 'MaterialIcons')),
              title: Text('Jadwal Belajar Bareng'),
            ),
            const ListTile(
              leading: Icon(IconData(58949, fontFamily: 'MaterialIcons')),
              title: Text('Notes'),
            ),
            const ListTile(
              leading: Icon(IconData(57634, fontFamily: 'MaterialIcons')),
              title: Text('Schedule Kuliah'),
            ),
            const ListTile(
              leading: Icon(IconData(58094, fontFamily: 'MaterialIcons')),
              title: Text('Study Communities'),
            ),
            const ListTile(
              leading: Icon(IconData(59045, fontFamily: 'MaterialIcons')),
              title: Text('Video Playlists'),
            ),
            const ListTile(
              leading: Icon(IconData(63098, fontFamily: 'MaterialIcons')),
              title: Text('Saran'),
            ),
            const ListTile(
              leading: Icon(IconData(58506, fontFamily: 'MaterialIcons')),
              title: Text('About Us'),
            ),
          ],
        ),
      ),

      // Body untuk isi card-card
      body: SingleChildScrollView(
          child: Column(children: <Widget>[
        // Header
        // https://educity.app/flutter/how-to-add-background-image-to-a-container-in-flutter
        Container(
            width: double.infinity,
            height: 300,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        "https://c.tenor.com/4ryx66tWEhcAAAAd/pixel-study.gif"),
                    // 'https://data.whicdn.com/images/304526132/original.gif'),
                    fit: BoxFit.cover)),
            padding: EdgeInsets.only(top: 50.0),
            child: Column(
              children: <Widget>[
                const Text(
                  "Agenda",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 64.0,
                      height: 1.2,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 10),
                const Text(
                  "Ada agenda apa hari ini?",
                  style: TextStyle(
                    color: Colors.white70,
                    fontSize: 18.0,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                const SizedBox(height: 50),
                ElevatedButton(
                  child: const Text(
                    'Tambahkan Agenda',
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue.shade700),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => const SecondRoute()),
                    // );
                  },
                ),
              ],
            )),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Laporan Tutorial 2 - Basdat'),
                subtitle: Text('23.55 - 19/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Lab 6 - PBP'),
                subtitle: Text('23.55 - 15/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Lab 6 - SDA'),
                subtitle: Text('23.55 - 16/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Research Proposal - MPPI'),
                subtitle: Text('23.55 - 26/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Tugas W09 - OS'),
                subtitle: Text('23.55 - 21/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),
      ])),
    );
  }
}
