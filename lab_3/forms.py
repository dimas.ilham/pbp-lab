# from django.db.models.fields import DateField
from django import forms
from django.forms import ModelForm
from lab_1.models import Friend

from django.utils.translation import gettext_lazy as _

# Create your models here.
class FriendForm(ModelForm):
    class Meta :
        model = Friend
        fields=['name', 'npm', 'dob']
        labels={
            'name':_('Nama'),
            'npm':_('NPM'),
            'dob':_('Tanggal Lahir')
        }
        widgets = {
            'dob': forms.SelectDateWidget(years=range(1990, 2011))
        }