from django.urls import path
from .views import add_friend, index

# To add path through the url
urlpatterns = [
    path('', index, name='index'),
    path('add', add_friend, name='add')
]