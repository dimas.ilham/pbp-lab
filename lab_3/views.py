from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

# index Function
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

#add_friend function
@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/lab-3')  # Redirect on finish
        
    else: # if a GET (or any other method) we'll create a blank form
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})