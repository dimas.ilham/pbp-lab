from django.shortcuts import render
from .models import Note

from django.http.response import HttpResponse
from django.core import serializers

# index Function
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

# XML Function
def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

# JSON Function
def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")