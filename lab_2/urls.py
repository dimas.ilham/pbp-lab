from django.urls import path
from .views import index
from .views import xml
from .views import json

# To add path through the url
urlpatterns = [
    path('', index, name='index'),
    path('xml/', xml, name = 'xml'),
    path('json/', json, name = 'json')
]