from django.db import models

# Create your models here.
class Note(models.Model):
    to_person = models.CharField(max_length=40)
    from_person = models.CharField(max_length=40)
    title = models.CharField(max_length=100)
    message = models.CharField(max_length=500)
