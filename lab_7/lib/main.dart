// @dart=2.9
import 'package:flutter/material.dart';
// import 'package:date_time_picker/date_time_picker.dart';

void main() {
  runApp(MaterialApp(
    title: 'Stu-Do-List',
    theme: ThemeData(
      scaffoldBackgroundColor: Colors.blue.shade50,
    ),
    home: MyCustomForm(),
  ));
}

class Matkul {
  const Matkul(this.name, this.value);
  final String name;
  final String value;
}

List<Matkul> namaMatkul = <Matkul>[
  const Matkul('Aljabar Linear', 'Alin'),
  const Matkul('Metodologi Penelitian dan Penulisan Ilmiah', 'MPPI'),
  const Matkul('Sistem Operasi', 'OS'),
  const Matkul('Pemrograman Berbasis Platform', 'PBP'),
  const Matkul('Struktur Data dan Algoritma', 'SDA'),
];

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  String _namaMatkul = "";
  String _namaTugas = "";
  String _tenggatWaktu = "";
  String _keterangan = "";

  Matkul selectedUser;

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
      appBar: AppBar(
        title: const Text('Stu-Do-List'),
        backgroundColor: Colors.grey[900],
        foregroundColor: Colors.white,
      ),
      // Sidebar menu
      // https://api.flutter.dev/flutter/material/Drawer-class.html
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.grey[900],
              ),
              child: const Text(
                'Stu-Do-List',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            const ListTile(
              leading: Icon(IconData(58136, fontFamily: 'MaterialIcons')),
              title: Text('Home'),
            ),
            const ListTile(
              leading: Icon(IconData(58950, fontFamily: 'MaterialIcons')),
              title: Text('Agenda'),
            ),
            const ListTile(
              leading: Icon(IconData(58618, fontFamily: 'MaterialIcons')),
              title: Text('Ask a Mentor'),
            ),
            const ListTile(
              leading: Icon(IconData(58710, fontFamily: 'MaterialIcons')),
              title: Text('Jadwal Belajar Bareng'),
            ),
            const ListTile(
              leading: Icon(IconData(58949, fontFamily: 'MaterialIcons')),
              title: Text('Notes'),
            ),
            const ListTile(
              leading: Icon(IconData(57634, fontFamily: 'MaterialIcons')),
              title: Text('Schedule Kuliah'),
            ),
            const ListTile(
              leading: Icon(IconData(58094, fontFamily: 'MaterialIcons')),
              title: Text('Study Communities'),
            ),
            const ListTile(
              leading: Icon(IconData(59045, fontFamily: 'MaterialIcons')),
              title: Text('Video Playlists'),
            ),
            const ListTile(
              leading: Icon(IconData(63098, fontFamily: 'MaterialIcons')),
              title: Text('Saran'),
            ),
            const ListTile(
              leading: Icon(IconData(58506, fontFamily: 'MaterialIcons')),
              title: Text('About Us'),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Card(
            margin: const EdgeInsets.all(40.0),
            child: Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: <Widget>[
                    // Judul
                    const Text(
                      "Tambah Agenda",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 32.0,
                          height: 1.6,
                          fontWeight: FontWeight.w800),
                      textAlign: TextAlign.center,
                    ),

                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // Field Nama Mata Kuliah
                          const SizedBox(height: 40),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 0),
                            child: Text(
                              "Nama Mata Kuliah",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: DropdownButtonFormField<Matkul>(
                              hint: Text("Pilih Mata Kuliah"),
                              value: selectedUser,
                              onChanged: (Matkul Value) {
                                setState(() {
                                  selectedUser = Value;
                                });
                              },
                              validator: (value) {
                                if (value == null) {
                                  setState(() => _namaMatkul = "Kosong");
                                  return 'Tolong Dipilih';
                                }
                                setState(() => _namaMatkul = value.name);
                                return null;
                              },
                              items: namaMatkul.map((Matkul matkul) {
                                return DropdownMenuItem<Matkul>(
                                  value: matkul,
                                  child: Row(
                                    children: <Widget>[
                                      // user.icon,
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        matkul.name,
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ],
                                  ),
                                );
                              }).toList(),
                            ),
                          ),

                          // Field Nama Keperluan
                          const SizedBox(height: 20),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 0),
                            child: Text(
                              "Nama Keperluan",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: TextFormField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(5.0)),
                                hintText: 'Tugas Pemrograman 3',
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  setState(() => _namaTugas = "Kosong");
                                  return 'Tolong Diisi';
                                }
                                setState(() => _namaTugas = value);
                                return null;
                              },
                            ),
                          ),

                          // Field Tenggat Waktu
                          const SizedBox(height: 20),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 0),
                            child: Text(
                              "Tenggat Waktu",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: TextFormField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(5.0)),
                                hintText: '23.55 30/11/2021',
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Tolong Diisi';
                                }
                                return null;
                              },
                            ),

                            // InputDatePickerFormField(

                            //     firstDate: DateTime.now(),
                            //     lastDate: DateTime(2100)),
                          ),

                          // Field keterangan tambahan
                          const SizedBox(height: 20),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 0),
                            child: Text(
                              "Keterangan Tambahan",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: TextFormField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(5.0)),
                                hintText:
                                    'Keterangan tambahan di lembar soal tugas',
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  setState(() => _keterangan = "Kosong");
                                  return 'Tolong Diisi';
                                }
                                setState(() => _keterangan = value);
                                return null;
                              },
                            ),
                          ),

                          const SizedBox(height: 30),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              ElevatedButton(
                                child: const Text(
                                  'Batal',
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.red.shade700),
                                  foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                ),
                                onPressed: () {/* ... */},
                              ),
                              const SizedBox(width: 10), // Jarak antarbutton
                              ElevatedButton(
                                child: const Text(
                                  'Tambah',
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.blue.shade700),
                                  foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                ),
                                onPressed: () {
                                  // If it's valid, get the value and print it to the console
                                  if (_formKey.currentState.validate()) {
                                    print("Nama Matkul : " + _namaMatkul);
                                    print("Nama Keperluan : " + _namaTugas);
                                    print("Tenggat Waktu : " + _tenggatWaktu);
                                    print("Keterangan : " + _keterangan + '\n');
                                  } else {
                                    print('belom lengkap\n');
                                  }
                                },
                              ),
                            ],
                          ),
                        ]),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
