import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PBP - Lab 6',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.grey,
        scaffoldBackgroundColor: Colors.blue.shade50,
        // scaffoldBackgroundColor: const Color(0xCCCCCCFF),
      ),
      home: const MyHomePage(title: 'Stu-Do-List'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // Return
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        backgroundColor: Colors.grey[900],
        foregroundColor: Colors.white,
      ),

      // Sidebar menu
      // https://api.flutter.dev/flutter/material/Drawer-class.html
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.grey[900],
              ),
              child: const Text(
                'Stu-Do-List',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            const ListTile(
              leading: Icon(IconData(58136, fontFamily: 'MaterialIcons')),
              title: Text('Home'),
            ),
            const ListTile(
              leading: Icon(IconData(58950, fontFamily: 'MaterialIcons')),
              title: Text('Agenda'),
            ),
            const ListTile(
              leading: Icon(IconData(58618, fontFamily: 'MaterialIcons')),
              title: Text('Ask a Mentor'),
            ),
            const ListTile(
              leading: Icon(IconData(58710, fontFamily: 'MaterialIcons')),
              title: Text('Jadwal Belajar Bareng'),
            ),
            const ListTile(
              leading: Icon(IconData(58949, fontFamily: 'MaterialIcons')),
              title: Text('Notes'),
            ),
            const ListTile(
              leading: Icon(IconData(57634, fontFamily: 'MaterialIcons')),
              title: Text('Schedule Kuliah'),
            ),
            const ListTile(
              leading: Icon(IconData(58094, fontFamily: 'MaterialIcons')),
              title: Text('Study Communities'),
            ),
            const ListTile(
              leading: Icon(IconData(59045, fontFamily: 'MaterialIcons')),
              title: Text('Video Playlists'),
            ),
            const ListTile(
              leading: Icon(IconData(63098, fontFamily: 'MaterialIcons')),
              title: Text('Saran'),
            ),
            const ListTile(
              leading: Icon(IconData(58506, fontFamily: 'MaterialIcons')),
              title: Text('About Us'),
            ),
          ],
        ),
      ),

      // Body untuk isi card-card
      body: SingleChildScrollView(
          child: Column(children: <Widget>[
        // Header
        // https://educity.app/flutter/how-to-add-background-image-to-a-container-in-flutter
        Container(
            width: double.infinity,
            height: 300,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        "https://c.tenor.com/4ryx66tWEhcAAAAd/pixel-study.gif"),
                    // 'https://data.whicdn.com/images/304526132/original.gif'),
                    fit: BoxFit.cover)),
            padding: EdgeInsets.only(top: 50.0),
            child: Column(
              children: <Widget>[
                const Text(
                  "Agenda",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 64.0,
                      height: 1.2,
                      fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 10),
                const Text(
                  "Ada agenda apa hari ini?",
                  style: TextStyle(
                    color: Colors.white70,
                    fontSize: 18.0,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                const SizedBox(height: 50),
                ElevatedButton(
                  child: const Text(
                    'Tambahkan Agenda',
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue.shade700),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => const SecondRoute()),
                    // );
                  },
                ),
              ],
            )),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Laporan Tutorial 2 - Basdat'),
                subtitle: Text('23.55 - 19/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Lab 6 - PBP'),
                subtitle: Text('23.55 - 15/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Lab 6 - SDA'),
                subtitle: Text('23.55 - 16/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Research Proposal - MPPI'),
                subtitle: Text('23.55 - 26/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),

        Card(
          margin: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const ListTile(
                leading: Icon(IconData(57585, fontFamily: 'MaterialIcons')),
                title: Text('Tugas W09 - OS'),
                subtitle: Text('23.55 - 21/11/2021'),
                // isThreeLine: true,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child: const Text(
                      'Hapus',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ],
          ),
        ),
      ])),
    );
  }
}
