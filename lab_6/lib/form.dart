import 'package:flutter/material.dart';

// void main() {
//   runApp(const MyApp());
// }

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stu-Do-List',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Stu-Do-List'),
          backgroundColor: Colors.grey[900],
          foregroundColor: Colors.white,
        ),
        // Sidebar menu
        // https://api.flutter.dev/flutter/material/Drawer-class.html
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.grey[900],
                ),
                child: const Text(
                  'Stu-Do-List',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              const ListTile(
                leading: Icon(IconData(58136, fontFamily: 'MaterialIcons')),
                title: Text('Home'),
              ),
              const ListTile(
                leading: Icon(IconData(58950, fontFamily: 'MaterialIcons')),
                title: Text('Agenda'),
              ),
              const ListTile(
                leading: Icon(IconData(58618, fontFamily: 'MaterialIcons')),
                title: Text('Ask a Mentor'),
              ),
              const ListTile(
                leading: Icon(IconData(58710, fontFamily: 'MaterialIcons')),
                title: Text('Jadwal Belajar Bareng'),
              ),
              const ListTile(
                leading: Icon(IconData(58949, fontFamily: 'MaterialIcons')),
                title: Text('Notes'),
              ),
              const ListTile(
                leading: Icon(IconData(57634, fontFamily: 'MaterialIcons')),
                title: Text('Schedule Kuliah'),
              ),
              const ListTile(
                leading: Icon(IconData(58094, fontFamily: 'MaterialIcons')),
                title: Text('Study Communities'),
              ),
              const ListTile(
                leading: Icon(IconData(59045, fontFamily: 'MaterialIcons')),
                title: Text('Video Playlists'),
              ),
              const ListTile(
                leading: Icon(IconData(63098, fontFamily: 'MaterialIcons')),
                title: Text('Saran'),
              ),
              const ListTile(
                leading: Icon(IconData(58506, fontFamily: 'MaterialIcons')),
                title: Text('About Us'),
              ),
            ],
          ),
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Center(
      child: Card(
        margin: const EdgeInsets.all(40.0),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              // Judul
              const Text(
                "Tambah Agenda",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 32.0,
                    height: 1.6,
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
              // Field Nama Mata Kuliah
              const SizedBox(height: 30),
              const Text(
                "Nama Mata Kuliah",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'SDA',
                  ),
                ),
              ),

              // Field Nama Keperluan
              const SizedBox(height: 20),
              const Text(
                "Nama Keperluan",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Tugas Pemrograman 3',
                  ),
                ),
              ),

              // Field Tenggat Waktu
              const SizedBox(height: 20),
              const Text(
                "Tenggat Waktu",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: '23.55 30/11/2021',
                  ),
                ),
              ),

              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ElevatedButton(
                    child: const Text(
                      'Batal',
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.red.shade700),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                    onPressed: () {/* ... */},
                  ),
                  const SizedBox(width: 10), // Jarak antarbutton
                  ElevatedButton(
                    child: const Text(
                      'Tambah',
                    ),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.blue.shade700),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                    ),
                    onPressed: () {/* ... */},
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
    // return Form(
    //   key: _formKey,
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.start,
    //     children: [
    //       TextFormField(
    //         // The validator receives the text that the user has entered.
    //         validator: (value) {
    //           if (value == null || value.isEmpty) {
    //             return 'Please enter some text';
    //           }
    //           return null;
    //         },
    //       ),
    //       Padding(
    //         padding: const EdgeInsets.symmetric(vertical: 16.0),
    //         child: ElevatedButton(
    //           onPressed: () {
    //             // Validate returns true if the form is valid, or false otherwise.
    //             if (_formKey.currentState!.validate()) {
    //               // If the form is valid, display a snackbar. In the real world,
    //               // you'd often call a server or save the information in a database.
    //               ScaffoldMessenger.of(context).showSnackBar(
    //                 const SnackBar(content: Text('Processing Data')),
    //               );
    //             }
    //           },
    //           child: const Text('Submit'),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
